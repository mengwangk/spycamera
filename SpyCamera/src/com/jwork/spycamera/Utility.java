package com.jwork.spycamera;

import java.util.List;

import android.hardware.Camera;
import android.hardware.Camera.Size;

public class Utility {
	
	public static String[] cameraSizeSupport(Camera camera, StringBuffer data) {
		List<Size> sizes = camera.getParameters().getSupportedPreviewSizes();
		List<Size> sizes2 = camera.getParameters().getSupportedPictureSizes();
		String[] imageList = new String[sizes.size()+sizes2.size()];
		int n = 0;
		for (Size size:sizes2) {
			imageList[n] = size.width + "x" + size.height+"*";
			data.append(imageList[n]);
			data.append("#");
			n++;
		}
		for (Size size:sizes) {
			imageList[n] = size.width + "x" + size.height;
			data.append(imageList[n]);
			if (n!=sizes.size()+sizes2.size()) {
				data.append("#");
			}
			n++;
		}
		return imageList;
	}
	
}
